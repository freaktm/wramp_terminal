//Aaron Storey
//
//module to read scan codes via ps/2 and process them.
//the rules for processing are as follows :
//
//if a normal key is pressed it is send to the fifo buffer as a code for external processing
//when a break code for a normal key is recieved, nothing is done.
//when a scan code for the caps lock key is recieved nothing is done.
//when a break code for the caps lock key is recieved, the caps lock register is inverted
//when a scan code for a shift key is recieved, the relevant shift register is set to '1'
//when a break code for a shift key is recieved, the relevant shift register is set to '0'
//nothing is done when any code for the alt buttons, ctrl buttons or the Function buttons is recieved.
//nothing is done when e0 is recieved.
module kb_code
   #(parameter W_SIZE = 2)  // 2^W_SIZE words in FIFO
   (
    input wire clk, reset,
    input wire ps2d, ps2c, rd_key_code,
    output wire [7:0] key_code,
    output wire kb_buf_empty,
	 output wire use_caps,
	 output wire shift_on
   );

   // constant declaration for control scan codes
   localparam 	BRK = 8'hf0,			// break code
					SPECIAL = 8'he0,		// special key prefix
					ALT = 8'h11,			// left alt and suffix of right alt
					CTRL = 8'h14,			// left ctrl and suffix of right ctrl
					TAB = 8'h0d,			// tab
					LSHIFT = 8'h12, 		// left shift
					RSHIFT = 8'h59, 		// right shift
					CAPS_LOCK = 8'h58,	// caps lock
					TILDE = 8'h0e; 		//tilde
	

   // symbolic state declaration
   localparam
		get_scan_code = 1'b0,
		get_brk_code = 1'b1;

   // signal declaration
	reg state_reg, state_next;
	reg caps_reg, caps_next;
	reg left_shift_reg, left_shift_next;
	reg right_shift_reg, right_shift_next;
   wire [7:0] scan_out;
   reg got_code_tick;
   wire scan_done_tick;
	wire shift_on_int;

	//wires for caps lock and shift state output
	assign shift_on_int = left_shift_reg | right_shift_reg;
	assign shift_on = shift_on_int;
	assign use_caps = caps_reg ^ shift_on_int;


   // instantiate ps2 receiver
   ps2_rx ps2_rx_unit
      (.clk(clk), .reset(reset), .rx_en(1'b1),
       .ps2d(ps2d), .ps2c(ps2c),
       .rx_done_tick(scan_done_tick), .dout(scan_out));

   // instantiate fifo buffer
   fifo #(.B(8), .W(W_SIZE)) fifo_key_unit
     (.clk(clk), .reset(reset), .rd(rd_key_code),
      .wr(got_code_tick), .w_data(scan_out),
      .empty(kb_buf_empty), .full(),
      .r_data(key_code));
		
	//=======================================================
   // FSM to process scan codes
   //=======================================================
   // state registers
   always @(posedge clk, posedge reset)
      if (reset)
			begin
				state_reg <= get_scan_code;
				caps_reg <= 1'b0;
				left_shift_reg <= 1'b0;
				right_shift_reg <= 1'b0;
			end
      else
			begin
				state_reg <= state_next;
				caps_reg <= caps_next;
				left_shift_reg <= left_shift_next;
				right_shift_reg <= right_shift_next;
			end
	

   // next-state logic
   always @*
   begin
      got_code_tick = 1'b0;
      state_next = state_reg;
		left_shift_next = left_shift_reg;
		right_shift_next = right_shift_reg;
		caps_next = caps_reg;
      case (state_reg)
         get_scan_code:  // wait for a new scan code and then process
            if (scan_done_tick==1'b1 && scan_out==BRK)				
               state_next = get_brk_code;	
				else if (scan_done_tick==1'b1 && scan_out==LSHIFT)
					left_shift_next = 1'b1;
				else if (scan_done_tick==1'b1 && scan_out==RSHIFT)
					right_shift_next = 1'b1;
				else if (scan_done_tick==1'b1 && (scan_out==TAB || scan_out==TILDE))
					got_code_tick = 1'b1;
            else if (scan_done_tick==1'b1 && scan_out!=ALT && scan_out!=CTRL && scan_out!=SPECIAL && scan_out!=CAPS_LOCK && scan_out[7:4]!=4'b0000)		
               got_code_tick = 1'b1;
         get_brk_code:  // wait for 2nd half of break code and process
				if (scan_done_tick==1'b1 && scan_out==LSHIFT)
					begin
						left_shift_next = 1'b0;
						state_next = get_scan_code;
					end
				else if (scan_done_tick==1'b1 && scan_out==RSHIFT)
					begin
						right_shift_next = 1'b0;
						state_next = get_scan_code;
					end
				else if (scan_done_tick==1'b1 && scan_out==CAPS_LOCK)
					begin
						caps_next = ~caps_reg;
						state_next = get_scan_code;
					end
				else if (scan_done_tick==1'b1)
					state_next = get_scan_code;
      endcase
   end

endmodule

