//module to emulate a vt100 terminal
//Designed by Aaron Storey
module terminal
   (
    input wire clk, reset,
	 input wire ps2c,
	 input wire ps2d,
	 input wire rx,
	 output wire tx,
    output wire hsync, vsync,
    output wire [7:0] rgb,
	 output wire bell,
	 output wire locked
   );

   // vga signals
   wire [9:0] pixel_x, pixel_y;
   wire video_on, pixel_tick;
   reg vga_bit_reg;
   wire vga_bit_next;
	wire [7:0] ascii_code;
	wire [6:0] ascii_char;
	wire ascii_not_ready;
	wire ascii_ready;
	wire vga_clk;
	wire logic_clk;
	
	

	
	//DCM
	logic_clk logic_clock (
		.CLKIN_IN(clk),
		.CLKIN_IBUFG_OUT(),
		.CLK0_OUT(logic_clk),
		.CLK2X_OUT(),
		.LOCKED_OUT(locked));
	
   // vga sync circuit
   vga_sync vsync_unit
      (.clk(logic_clk), .reset(reset), .hsync(hsync), .vsync(vsync),
       .video_on(video_on), .p_tick(pixel_tick),
       .pixel_x(pixel_x), .pixel_y(pixel_y));
		 
   // font generation circuit
   screen_controller screen_controller(
		.clk(logic_clk),
		.reset(reset),
		.video_on(video_on),
		.ascii(ascii_char),
		.ascii_ready(ascii_ready),
		.pixel_x(pixel_x),
		.pixel_y(pixel_y),
		.text_bit(vga_bit_next),
		.bell(bell));


	//keyboard to uart module (19200 baud rate, no parity)
	kb_controller kb_controller(
		.clk(logic_clk),
		.reset(reset),
		.ps2d(ps2d),
		.ps2c(ps2c),
		.tx(tx),
		.rx(rx),
		.ascii_not_ready(ascii_not_ready), 
		.ascii(ascii_code));

	//not using extended ascii
	assign ascii_char = ascii_code[6:0];
	
	//signal to only update ram if new char recieved on serial
	assign ascii_ready = ~ascii_not_ready;
	 
		 
   // vga bit buffer
   always @(posedge logic_clk)
		begin
			if (pixel_tick)
				vga_bit_reg <= vga_bit_next;
		end
			
   // vga output
   assign rgb[0] = vga_bit_reg;
   assign rgb[1] = vga_bit_reg;
   assign rgb[2] = vga_bit_reg;
   assign rgb[3] = vga_bit_reg;
   assign rgb[4] = vga_bit_reg;
   assign rgb[5] = vga_bit_reg;
   assign rgb[6] = vga_bit_reg;	
   assign rgb[7] = vga_bit_reg;
	
	
endmodule
