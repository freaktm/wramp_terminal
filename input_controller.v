`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:31:31 06/24/2014 
// Design Name: 
// Module Name:    input_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module input_controller(
    input clk,
	 input reset,
    input ascii_ready,
    input [6:0] ascii_code,
	 input [6:0] read_data,
    output [6:0] write_data,
	 output [6:0] cur_x,
	 output [6:0] cur_y,
    output we,
    output [11:0] write_addr,
	 output bell,
	 output visible
    );

   // constant declaration for ascii control codes
   localparam 	BELL = 7'h07,			// bell
					SPACE = 7'h20,			// space
					BACKSPACE = 7'h08,	// backspace
					DEL = 7'h7f,			// delete
					TAB = 7'h09,			// tab
					LF = 7'h0a, 			// line feed, new line
					CR = 7'h0d, 			// carriage return
					ESC = 7'h1b, 			// escape
					BRACKET = 7'h5b;		// [
	

   // symbolic state declaration
   localparam
		get_ascii_code = 5'b00000,
		save_ascii_to_ram_cfg = 5'b00001,
		save_ascii_to_ram = 5'b00010,
		do_backspace = 5'b00011,
		do_delete = 5'b00100,
		do_tab = 5'b00101,
		do_lf = 5'b00110,
		do_cr = 5'b00111,
		do_esc = 5'b01000,
		shift_row_left = 5'b01001,
		shift_row_left_setup = 5'b01010,
		shift_row_left_write = 5'b01011,
		write_space = 5'b01100,
		get_bracket = 5'b01101,
		get_p1 = 5'b01110,
		get_p2 = 5'b01111,
		get_p3 = 5'b10000,
		get_p4 = 5'b10001,
		get_5 = 5'b10010,
		get_visible = 5'b10011,
		move_top_left = 5'b10100,
		clear_screen = 5'b10101,
		get_2 = 5'b10110,
		get_seperator = 5'b10111,
		get_H = 5'b11000,
		move_location = 5'b11001;

   // 80-by-30 tile map
   localparam MAX_X = 80;
   localparam MAX_Y = 30;	
	
   // cursor position
   reg [6:0] cur_x_reg;
   reg [6:0] cur_x_next;
	reg [6:0] cur_x_temp;
   reg [6:0] cur_y_reg;
   reg [6:0] cur_y_next; 
	reg [6:0] cur_y_temp;
	reg visible_reg, visible_next;
	assign visible = visible_reg;
	
	assign cur_x = cur_x_reg;
	assign cur_y = cur_y_reg;

	//state registers
	reg [4:0] state_reg, state_next;
	reg [6:0] current_code;
	reg [6:0] p1;
	reg [6:0] p2;
	reg [6:0] p3;
	reg [6:0] p4;
	
	//ram
	reg write_e;	
	assign we = write_e;
	reg [11:0] address;
	assign write_addr = address;
	assign write_data = current_code;
	
	
	//bell
	reg do_bell;
	assign bell = do_bell;

	/////////////////////////
	/// BEHAVIOURAL
	/////////////////////////

	//=======================================================
   // FSM to process ascii codes
   //=======================================================
   // state registers
   always @(posedge clk, posedge reset)
      if (reset)
			begin
				state_reg <= get_ascii_code;
				cur_x_reg <= 7'b0000000;
				cur_y_reg <= 5'b00000;
				visible_reg <= 1'b1;
			end
      else
			begin
				state_reg <= state_next;
				cur_x_reg <= cur_x_next;
				cur_y_reg <= cur_y_next;
				visible_reg <= visible_next;
			end


// next-state logic OUCH !
   always @(posedge clk, posedge reset)
	if (reset)
		begin
			cur_x_temp <= 7'b0000000;
			cur_y_temp <= 5'b00000;
			current_code <= 7'b0000000;
			address <= 12'h000;
			p1 <= 7'h00;
			p2 <= 7'h00;
			p3 <= 7'h00;
			p4 <= 7'h00;
		end
	else
		begin
			do_bell = 1'b0;
			write_e = 1'b0;
			state_next = state_reg;
			cur_x_next = cur_x_reg;
			cur_y_next = cur_y_reg;
			visible_next = visible_reg;
			case (state_reg)
				get_ascii_code:  // wait for a new ascii code and then process
					if (ascii_ready==1'b1 && ascii_code > 8'h1f && ascii_code < 8'h7f)	
						begin
							state_next = save_ascii_to_ram_cfg;
							current_code <= ascii_code;
						end
					else if (ascii_ready==1'b1 && ascii_code==BELL)
						begin
							do_bell = 1'b1;	
							state_next = get_ascii_code;
						end
					else if (ascii_ready==1'b1 && ascii_code==BACKSPACE)
						begin
							state_next = do_backspace;
						end
					else if (ascii_ready==1'b1 && ascii_code==DEL)
						begin
							state_next = do_delete;
						end
					else if (ascii_ready==1'b1 && ascii_code==TAB)
						begin
							state_next = do_tab;
						end
					else if (ascii_ready==1'b1 && ascii_code==LF)
						begin
							state_next = do_lf;
						end
					else if (ascii_ready==1'b1 && ascii_code==CR)
						begin
							state_next = do_cr;
						end
					else if (ascii_ready==1'b1 && ascii_code==ESC)
						begin
							state_next = do_esc;
						end
				save_ascii_to_ram_cfg:  // save ascii to ram address setup
					begin
						address <= {cur_y_reg, cur_x_reg};
						state_next = save_ascii_to_ram;
					end
				save_ascii_to_ram:	
					begin
						write_e = 1'b1;
						//move right NOT IMPLEMNTED
						cur_x_next = (cur_x_reg+1<MAX_X) ?  cur_x_reg +1 : 0;
						state_next = get_ascii_code;
					end
				do_backspace:
					begin
						if(cur_x_reg > 7'b0000000)
							begin
								cur_y_temp <= cur_y_reg;
								cur_x_temp <= cur_x_reg;
								cur_x_next = cur_x_next - 1;
								state_next = shift_row_left;
							end
						else
							state_next = get_ascii_code;
					end
				shift_row_left:
					begin
						if(cur_x_temp<MAX_X)
							begin							
								address <= {cur_y_temp, cur_x_temp};
								state_next = shift_row_left_setup;
							end
						else
							begin
								address <= {cur_y_temp, MAX_X-1};
								current_code <= SPACE;
								state_next = write_space;
							end
					end
				shift_row_left_setup:
					begin
						current_code <= read_data;							
						address <= {cur_y_temp, cur_x_temp - 1};
						state_next = shift_row_left_write;
					end
				shift_row_left_write:
					begin
						write_e = 1'b1;
						cur_x_temp <= cur_x_temp + 1;
						state_next = shift_row_left;
					end
				write_space:
					begin
						write_e = 1'b1;
						state_next = get_ascii_code;
					end
				do_delete:
					begin
						cur_y_temp <= cur_y_reg;
						cur_x_temp <= cur_x_reg + 1;
						state_next = shift_row_left;
					end
				do_tab:  //NOT IMPLEMENTED
					begin
						state_next = get_ascii_code;
					end
				do_lf:  //NOT IMPLEMENTED
					begin
						state_next = get_ascii_code;
					end
				do_cr:  //NOT IMPLEMENTED
					begin
						state_next = get_ascii_code;
					end
				do_esc: //get bracket
					begin
						if(ascii_ready==1'b1 && ascii_code==BRACKET)
							state_next=get_p1;
						else if(ascii_ready==1'b1 && ascii_code!=ESC)
							state_next=get_ascii_code;							
					end
				get_p1: //get first character after [
					begin
						if(ascii_ready==1'b1 && ascii_code!=BRACKET)
							case (ascii_code)
								7'h48: 	//H
									begin
										state_next = move_top_left;
									end
								7'h3f:	//?
									begin
										state_next = get_2;
									end
								default:
									begin
										if (ascii_code > 7'h2f && ascii_code < 7'h3a)
											begin
												p1 <= ascii_code;
												state_next = get_p2;												
											end
										else
											state_next = get_ascii_code;
									end
							endcase
							//check for different valid cases
					end
				get_p2: 
					begin
						if(ascii_ready==1'b1)
							begin
								case (ascii_code)
									7'h4a: 	//J
										begin
											if(p1==7'h32)
												state_next = clear_screen;
											else
												state_next = get_ascii_code;
										end
									default:
										begin
											if (ascii_code > 7'h2f && ascii_code < 7'h3a)
												begin
													p2 <= ascii_code;
													state_next = get_seperator;												
												end
											else
												state_next = get_ascii_code;
										end
								endcase
							end
					end
				get_p3:
					begin
						if(ascii_ready==1'b1 && ascii_code > 7'h2f && ascii_code < 7'h3a)
							begin
								p3 <= ascii_code;
								state_next = get_p4;
							end
						else if (ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				get_p4:
					begin
						if(ascii_ready==1'b1 && ascii_code > 7'h2f && ascii_code < 7'h3a)
							begin
								p4 <= ascii_code;
								state_next = get_H;
							end
						else if (ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				get_seperator:
					begin
						if(ascii_ready==1'b1 && ascii_code==7'h3b)
							state_next = get_p3;
						else if(ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				clear_screen: //NOT IMPLEMENTED
					begin
						//is there a way to clear ram contents ?
						state_next = get_ascii_code;
					end
				move_top_left:
					begin
						cur_x_next = 7'h00;
						cur_y_next = 5'h00;
						state_next = get_ascii_code;
					end
				get_2:
					begin
						if(ascii_ready==1'b1 && ascii_code==7'h32)
							state_next = get_5;
						else if(ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				get_5:
					begin
						if(ascii_ready==1'b1 && ascii_code==7'h35)
							state_next = get_visible;
						else if(ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				get_H:
					begin
						if(ascii_ready==1'b1 && ascii_code==7'h48) //H
							begin
								state_next = move_location;
							end
						else if(ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				get_visible:
					begin
						if(ascii_ready==1'b1 && ascii_code==7'h6c)
							begin
								visible_next = 1'b0;
								state_next = get_ascii_code;
							end
						else if(ascii_ready==1'b1 && ascii_code==7'h68)
							begin
								visible_next = 1'b1;
								state_next = get_ascii_code;
							end
						else if(ascii_ready==1'b1)
							state_next = get_ascii_code;
					end
				move_location:
					begin
						//set cur_y_next to p1 and p2 digits as decimal
						//set cur_x_next to p2 and p3 digits as decimal
					end

			endcase
		end

endmodule
