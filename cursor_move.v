`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:52:47 06/22/2014 
// Design Name: 
// Module Name:    cursor_move 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cursor_move(
	 input [1:0] direction,
    input [6:0] cur_x_in,
    input [6:0] cur_y_in,
    output [6:0] cur_x_out,
    output [6:0] cur_y_out
    );


	localparam
		left = 2'b00,
		right = 2'b01,
		down = 2'b10,
		up = 2'b11;
		
		reg [6:0] cur_x, cur_y;
		
	assign cur_x_out = cur_x;
	assign cur_y_out = cur_y;

	always @*
		case(direction)
			left:
				begin
					cur_x = cur_x - 1;
				end
			right:
				begin
					cur_x = cur_x - 1;
				end
			down:
				begin
					cur_x = cur_x - 1;
				end
			up:
				begin
					cur_x = cur_x - 1;
				end
		endcase
		
		
endmodule
