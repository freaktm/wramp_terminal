//Listing 9.2
module kb_monitor
   (
    input wire clk, reset,
    input wire ps2d, ps2c,
	 input wire rx,
    output wire tx,
	 output wire [7:0] ascii	 
   );

   // constant declaration
   localparam SP=8'h20; // space in ASCII

   // symbolic state declaration
   localparam 
      idle  = 1'b0,
      send = 1'b1;

   // signal declaration
   reg state_reg, state_next;
   reg [7:0] w_data;
   wire [7:0] scan_data;
   reg wr_uart;
   wire scan_done_tick;
	wire [7:0] ascii_code;

   // body
   //====================================================
   // instantiation
   //====================================================
   // instantiate ps2 receiver
   ps2_rx ps2_rx_unit
      (.clk(clk), .reset(reset), .rx_en(1'b1),
       .ps2d(ps2d), .ps2c(ps2c),
       .rx_done_tick(scan_done_tick), .dout(scan_data));

   // instantiate UART
   uart uart_unit
      (.clk(clk), .reset(reset), .rd_uart(1'b0),
       .wr_uart(wr_uart), .rx(rx), .w_data(w_data),
       .tx_full(), .rx_empty(), .r_data(ascii), .tx(tx));
		 
	// unit to convert make code from keyboard into ascii code for transmit to PC
	key2ascii key2ascii(
		.key_code(scan_data),
		.ascii_code(ascii_code));
		 


   //====================================================
   // FSM to send ASCII characters
   //====================================================
   // state registers
   always @(posedge clk, posedge reset)
      if (reset)
         state_reg <= idle;
      else
         state_reg <= state_next;
			

   // next-state logic
   always @*
   begin
      wr_uart = 1'b0;
      w_data = SP;
      state_next = state_reg;
      case (state_reg)
         idle:
            if (scan_done_tick) // a scan code received
               state_next = send;
         send: // send higher hex char
            begin
               w_data = ascii_code;
               wr_uart = 1'b1;
               state_next = idle;
            end
      endcase
   end



endmodule