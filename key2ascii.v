//Module to convert keyboard scan code into ascii
//uses a caps_on signal and a shift_on signal to determine the state of caps lock and shift
//scan codes that are not defined are returned as 0 for the ascii code (null).
module key2ascii
   (
    input wire [7:0] key_code,
	 input wire use_caps,
	 input wire shift_on,
    output reg [7:0] ascii_code	 
   );
	
always @*
   case(key_code)
       8'h45: ascii_code = (shift_on) ? 8'h29 : 8'h30;   // )0
       8'h16: ascii_code = (shift_on) ? 8'h21 : 8'h31;   // !1
       8'h1e: ascii_code = (shift_on) ? 8'h40 : 8'h32;   // @2
       8'h26: ascii_code = (shift_on) ? 8'h23 : 8'h33;   // #3
       8'h25: ascii_code = (shift_on) ? 8'h24 : 8'h34;   // $4
       8'h2e: ascii_code = (shift_on) ? 8'h25 : 8'h35;   // %5
       8'h36: ascii_code = (shift_on) ? 8'h5e : 8'h36;   // ^6
       8'h3d: ascii_code = (shift_on) ? 8'h26 : 8'h37;   // &7
       8'h3e: ascii_code = (shift_on) ? 8'h2a : 8'h38;   // *8
       8'h46: ascii_code = (shift_on) ? 8'h28 : 8'h39;   // (9

       8'h1c: ascii_code = (use_caps) ? 8'h41 : 8'h61;   // Aa
       8'h32: ascii_code = (use_caps) ? 8'h42 : 8'h62;   // Bb
       8'h21: ascii_code = (use_caps) ? 8'h43 : 8'h63;   // Cc
       8'h23: ascii_code = (use_caps) ? 8'h44 : 8'h64;   // Dd
       8'h24: ascii_code = (use_caps) ? 8'h45 : 8'h65;   // Ee
       8'h2b: ascii_code = (use_caps) ? 8'h46 : 8'h66;   // Ff
       8'h34: ascii_code = (use_caps) ? 8'h47 : 8'h67;   // Gg
       8'h33: ascii_code = (use_caps) ? 8'h48 : 8'h68;   // Hh
       8'h43: ascii_code = (use_caps) ? 8'h49 : 8'h69;   // Ii
       8'h3b: ascii_code = (use_caps) ? 8'h4a : 8'h6a;   // Jj
       8'h42: ascii_code = (use_caps) ? 8'h4b : 8'h6b;   // Kk
       8'h4b: ascii_code = (use_caps) ? 8'h4c : 8'h6c;   // Ll
       8'h3a: ascii_code = (use_caps) ? 8'h4d : 8'h6d;   // Mm
       8'h31: ascii_code = (use_caps) ? 8'h4e : 8'h6e;   // Nn
       8'h44: ascii_code = (use_caps) ? 8'h4f : 8'h6f;   // Oo
       8'h4d: ascii_code = (use_caps) ? 8'h50 : 8'h70;   // Pp
       8'h15: ascii_code = (use_caps) ? 8'h51 : 8'h71;   // Qq
       8'h2d: ascii_code = (use_caps) ? 8'h52 : 8'h72;   // Rr
       8'h1b: ascii_code = (use_caps) ? 8'h53 : 8'h73;   // Ss
       8'h2c: ascii_code = (use_caps) ? 8'h54 : 8'h74;   // Tt
       8'h3c: ascii_code = (use_caps) ? 8'h55 : 8'h75;   // Uu
       8'h2a: ascii_code = (use_caps) ? 8'h56 : 8'h76;   // Vv
       8'h1d: ascii_code = (use_caps) ? 8'h57 : 8'h77;   // Ww
       8'h22: ascii_code = (use_caps) ? 8'h58 : 8'h78;   // Xx
       8'h35: ascii_code = (use_caps) ? 8'h59 : 8'h79;   // Yy
       8'h1a: ascii_code = (use_caps) ? 8'h5a : 8'h7a;   // Zz

       8'h0e: ascii_code = (shift_on) ? 8'h7e : 8'h60;   // ~`
       8'h4e: ascii_code = (shift_on) ? 8'h5f : 8'h2d;   // _-
       8'h55: ascii_code = (shift_on) ? 8'h2b : 8'h3d;   // +=
       8'h54: ascii_code = (shift_on) ? 8'h7b : 8'h5b;   // {[
       8'h5b: ascii_code = (shift_on) ? 8'h7d : 8'h5d;   // }]
       8'h5d: ascii_code = (shift_on) ? 8'h7c : 8'h5c;   // |\
       8'h4c: ascii_code = (shift_on) ? 8'h3a : 8'h3b;   // :;
       8'h52: ascii_code = (shift_on) ? 8'h22 : 8'h27;   // "'
       8'h41: ascii_code = (shift_on) ? 8'h3c : 8'h2c;   // <,
       8'h49: ascii_code = (shift_on) ? 8'h3e : 8'h2e;   // >.
       8'h4a: ascii_code = (shift_on) ? 8'h3f : 8'h2f;   // ?/

       8'h29: ascii_code = 8'h20;   							// (space)
       8'h5a: ascii_code = 8'h0d;   							// (enter, cr)
       8'h66: ascii_code = 8'h08;   							// (backspace)
		 8'h0d: ascii_code = 8'h09;								// (tab)
		 8'h76: ascii_code = 8'h1b;								// (esc)
		 
       default: ascii_code = 8'h00; 							// (null)  DEFAULT
    endcase

endmodule